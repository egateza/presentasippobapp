/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;


import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.jpos.iso.*;

import org.jpos.iso.packager.*;
import org.jpos.util.Log;

/**
 *
 * @author egateza
 */
public class ParseISOMessage {
    
    
    public Map<String, String> getBitmap(String data) {
        System.out.println("=============== getting bitmap =========");
        Map<String, String> bitMap = new HashMap<String, String>();
        try {
//            GenericPackager packager = new GenericPackager("iso87ascii.xml");
            GenericPackager packager = new GenericPackager("packager/isoSLSascii.xml");
//            GenericPackager newpackager = new GenericPackager("packager/iso87ascii_minipack.xml");
            ISOMsg isoMsg = new ISOMsg();
//            ISOMsg isoMsgNew = new ISOMsg();
//            isoMsgNew.setPackager(newpackager);
            
            isoMsg.setPackager(packager);
            isoMsg.unpack(data.getBytes());
            System.out.println("======> "+isoMsg.getValue(-1));
            String bitmapString = isoMsg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] bitmapArray = bitmapString.split(",");
            System.out.println("bitmapArray :: "+Arrays.toString(bitmapArray));
            System.out.println("=================DATA FIELD PARSING=====================");
            bitMap.put("MTI", isoMsg.getMTI());
            System.out.println("getting MTI from bitmap :: "+bitMap.get("MTI"));
            System.out.println("bitmapArray length :: "+bitmapArray.length);
//            isoMsgNew.setMTI(isoMsg.getMTI());
            for (int i = 0; i < bitmapArray.length; i++) {
                String bitId = bitmapArray[i].trim();
//                System.out.println("bit :: "+bitId+" "+isoMsg.getString(bitId));
                bitMap.put(bitId, isoMsg.getString(bitId));
                System.out.println("BIT " + bitId + " : " + bitMap.get(bitId));
//                isoMsgNew.set(bitId, bitMap.get(bitId));
//                System.out.println(Arrays.toString(bitMap));
            }
//            byte[] datax = isoMsgNew.pack();
//            System.out.println(":: NEW ISO :: "+new String(datax));
            
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        
        return bitMap;
    }
    
}
