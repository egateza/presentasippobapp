/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

/**
 *
 * @author ngonar
 */
@Entity
@Table(name = "mutations")
@NamedQueries({
    @NamedQuery(name = "Mutations.findAll", query = "SELECT m FROM Mutations m"),
    @NamedQuery(name = "Mutations.getCurrentBalance", query = "SELECT sum(m.amount) FROM Mutations m WHERE m.userId = :userId"),
    @NamedQuery(name = "Mutations.findById", query = "SELECT m FROM Mutations m WHERE m.id = :id"),
    @NamedQuery(name = "Mutations.findByAmount", query = "SELECT m FROM Mutations m WHERE m.amount = :amount"),
    @NamedQuery(name = "Mutations.findByNote", query = "SELECT m FROM Mutations m WHERE m.note = :note"),
    @NamedQuery(name = "Mutations.findByJenis", query = "SELECT m FROM Mutations m WHERE m.jenis = :jenis"),
    @NamedQuery(name = "Mutations.findByInboxId", query = "SELECT m FROM Mutations m WHERE m.inboxId = :inboxId"),
    @NamedQuery(name = "Mutations.findByBalance", query = "SELECT m FROM Mutations m WHERE m.balance = :balance"),
    @NamedQuery(name = "Mutations.findByCreateDate", query = "SELECT m FROM Mutations m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "Mutations.findByCreateBy", query = "SELECT m FROM Mutations m WHERE m.createBy = :createBy"),
    @NamedQuery(name = "Mutations.findByUpdateDate", query = "SELECT m FROM Mutations m WHERE m.updateDate = :updateDate"),
    @NamedQuery(name = "Mutations.findByUpdateBy", query = "SELECT m FROM Mutations m WHERE m.updateBy = :updateBy"),
    @NamedQuery(name = "Mutations.findByUserId", query = "SELECT m FROM Mutations m WHERE m.userId = :userId")})

@SequenceGenerator(sequenceName="mutations_id_seq",name="mutasi_gen",allocationSize=1)
public class Mutations implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mutasi_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    @Version
    private long version;

    @Basic(optional = false)
    @Column(name = "amount")
    private long amount;
    @Basic(optional = false)
    @Column(name = "note")
    private String note;
    @Basic(optional = false)
    @Column(name = "jenis")
    private char jenis;
    @Basic(optional = false)
    @Column(name = "inbox_id")
    private int inboxId;
    @Basic(optional = false)
    @Column(name = "balance")
    private long balance;
    @Column(name = "create_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "create_by")
    private String createBy;
    @Column(name = "update_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "update_by")
    private String updateBy;
    @Column(name = "user_id")
    private Integer userId;

    public Mutations() {
    }

    public Mutations(Long id) {
        this.id = id;
    }

    public Mutations(Long id, long amount, String note, char jenis, int inboxId, long balance) {
        this.id = id;
        this.amount = amount;
        this.note = note;
        this.jenis = jenis;
        this.inboxId = inboxId;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public char getJenis() {
        return jenis;
    }

    public void setJenis(char jenis) {
        this.jenis = jenis;
    }

    public int getInboxId() {
        return inboxId;
    }

    public void setInboxId(int inboxId) {
        this.inboxId = inboxId;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(long balance) {
        this.balance = balance;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Mutations)) {
            return false;
        }
        Mutations other = (Mutations) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Mutations[id=" + id + "]";
    }

}
