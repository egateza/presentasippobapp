/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 *
 * @author egateza
 */
public class Settings {
    
    public static String connectionString = "";
    public static String connectionDriver = "";
    public static String connectionUser = "";
    public static String connectionPass = "";
    public static String rabbit_host = "";
    
    
    private static String gateway_host = "";
    private static String gateway_port = "";
    private static String gateway_sleep = "";
    
    private static String rabbit_exchange = "";
    private static String rabbit_queueName = "";
    private static String rabbit_bindingKey = "";

    private static String rabbit_hostOut = "";
    private static String rabbit_exchangeOut = "";
    private static String rabbit_queueNameOut = "";
    private static String rabbit_bindingKeyOut = "";
    
    public static int NTHREDS = 1;
    public static int chunk = 1;

    public static int getNTHREDS() {
        return NTHREDS;
    }

    public static void setNTHREDS(int NTHREDS) {
        Settings.NTHREDS = NTHREDS;
    }

    public static int getChunk() {
        return chunk;
    }

    public static void setChunk(int chunk) {
        Settings.chunk = chunk;
    }
    
    
    public static String getConnectionString() {
        return connectionString;
    }

    public static void setConnectionString(String connectionString) {
        Settings.connectionString = connectionString;
    }

    public static String getConnectionDriver() {
        return connectionDriver;
    }

    public static void setConnectionDriver(String connectionDriver) {
        Settings.connectionDriver = connectionDriver;
    }

    public static String getConnectionUser() {
        return connectionUser;
    }

    public static void setConnectionUser(String connectionUser) {
        Settings.connectionUser = connectionUser;
    }

    public static String getConnectionPass() {
        return connectionPass;
    }

    public static void setConnectionPass(String connectionPass) {
        Settings.connectionPass = connectionPass;
    }

    public static String getRabbit_host() {
        return rabbit_host;
    }

    public static void setRabbit_host(String rabbit_host) {
        Settings.rabbit_host = rabbit_host;
    }

    public static String getGateway_host() {
        return gateway_host;
    }

    public static void setGateway_host(String gateway_host) {
        Settings.gateway_host = gateway_host;
    }

    public static String getGateway_port() {
        return gateway_port;
    }

    public static void setGateway_port(String gateway_port) {
        Settings.gateway_port = gateway_port;
    }

    public static String getGateway_sleep() {
        return gateway_sleep;
    }

    public static void setGateway_sleep(String gateway_sleep) {
        Settings.gateway_sleep = gateway_sleep;
    }

    public static String getRabbit_exchange() {
        return rabbit_exchange;
    }

    public static void setRabbit_exchange(String rabbit_exchange) {
        Settings.rabbit_exchange = rabbit_exchange;
    }

    public static String getRabbit_queueName() {
        return rabbit_queueName;
    }

    public static void setRabbit_queueName(String rabbit_queueName) {
        Settings.rabbit_queueName = rabbit_queueName;
    }

    public static String getRabbit_bindingKey() {
        return rabbit_bindingKey;
    }

    public static void setRabbit_bindingKey(String rabbit_bindingKey) {
        Settings.rabbit_bindingKey = rabbit_bindingKey;
    }

    public static String getRabbit_hostOut() {
        return rabbit_hostOut;
    }

    public static void setRabbit_hostOut(String rabbit_hostOut) {
        Settings.rabbit_hostOut = rabbit_hostOut;
    }

    public static String getRabbit_exchangeOut() {
        return rabbit_exchangeOut;
    }

    public static void setRabbit_exchangeOut(String rabbit_exchangeOut) {
        Settings.rabbit_exchangeOut = rabbit_exchangeOut;
    }

    public static String getRabbit_queueNameOut() {
        return rabbit_queueNameOut;
    }

    public static void setRabbit_queueNameOut(String rabbit_queueNameOut) {
        Settings.rabbit_queueNameOut = rabbit_queueNameOut;
    }

    public static String getRabbit_bindingKeyOut() {
        return rabbit_bindingKeyOut;
    }

    public static void setRabbit_bindingKeyOut(String rabbit_bindingKeyOut) {
        Settings.rabbit_bindingKeyOut = rabbit_bindingKeyOut;
    }
    
    
    public void setSetting() {
        
        try {
            File f = new File("settings.properties");
            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);
                
                String rabbitHost = pro.getProperty("Pelangi.rabbit.host.in");
                String rabbitExchange = pro.getProperty("Pelangi.rabbit.exchange.in");
                String rabbitQueueName = pro.getProperty("Pelangi.rabbit.queueName.in");
                String rabbitBindingKey = pro.getProperty("Pelangi.rabbit.bindingKey.in");

                String rabbitHostOut = pro.getProperty("Pelangi.rabbit.host.out");
                String rabbitExchangeOut = pro.getProperty("Pelangi.rabbit.exchange.out");
                String rabbitQueueNameOut = pro.getProperty("Pelangi.rabbit.queueName.out");
                String rabbitBindingKeyOut = pro.getProperty("Pelangi.rabbit.bindingKey.out");
                
                setRabbit_hostOut(rabbitHostOut);
                setRabbit_bindingKeyOut(rabbitBindingKeyOut);
                setRabbit_exchangeOut(rabbitExchangeOut);
                setRabbit_queueNameOut(rabbitQueueNameOut);
                
                setRabbit_host(rabbitHost);
                setRabbit_bindingKey(rabbitBindingKey);
                setRabbit_exchange(rabbitExchange);
                setRabbit_queueName(rabbitQueueName);
                
                String gatewayHost = pro.getProperty("gateway.host");
                String gatewayPort = pro.getProperty("gateway.port");
                String gatewaySleep = pro.getProperty("gateway.sleep");
                
                setGateway_host(gatewayHost);
                setGateway_port(gatewayPort);
                setGateway_sleep(gatewaySleep);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Setting file not found");
        }
        
    }
}
