/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BPJS;

import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.packager.GenericPackager;
import static utility.HitGateway.printWriter;
import utility.ParseISOMessage;
import utility.Settings;

/**
 *
 * @author egateza
 */
public class Bpjs {
    private static Logger logger = Logger.getLogger(Bpjs.class);
    int PORT_SERVER = 12345;
    String SERVER_IP = "localhost";
    int sleepTime = 0;
    Settings setting = new Settings();
    public Bpjs() {
        this.PORT_SERVER = Integer.parseInt(setting.getGateway_port());
        this.SERVER_IP = setting.getGateway_host();
    }
    public String request_inquiry(String idpel, String periode, String user_id, String inbox_id) {
        String hasil = "";
        try {
            logger.info("Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            Socket client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);
            hasil = inquiry(idpel, periode, user_id, inbox_id, client);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasil;
    }
    
    public String inquiry(String idpel, String periode, String user_id, String inbox_id, Socket client) {
        String hasil = "";
        
        try {
            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            
            isoMsg.setMTI("2100");
            isoMsg.set(2, "99508");
//            isoMsg.set(2, "00339");
            isoMsg.set(11, String.format("%12s", inbox_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, "6021");
            isoMsg.set(32, "4510017");
            isoMsg.set(33, "4510206");
            isoMsg.set(41, "0000000000000048");
            
            
            String bit48 = "0000000"+String.format("%-18s", idpel)+ periode;
            isoMsg.set(48, bit48);
            String[] userData = getUserdata();
            String bit63 = String.format("%-32s", userData[0])
                    + String.format("%-30s", userData[1])
                    + String.format("%-50s", userData[2])
                    + String.format("%-18s", userData[3])
                    + String.format("%-4s", userData[6].replace(".", ""))
                    + String.format("%-30s", userData[5].replace(".", ""))
                    + String.format("%-10s", userData[7].replace(".", ""))
                    ;
            isoMsg.set(63, bit63);
            
            byte[] datax = isoMsg.pack();
            String networkRequest = new String(datax);
            hasil = printWriter(networkRequest);
            
//            String response = "211050300041828100020599508360000000024250000000000012320190611110728602107451001707451020800000000000000000048257JTL53L30000002426690250  01OIEJ HAIJ LIEN       (PST:  1)1101  SEMARANG                                                                                            000000240000000000002500000000242500000000000000CE431880DA7F588F                20180409080812174pids16060001                    egateza                       Kp. Kadubuluh Desa Sukasari Kecamatan Kaduhejo Pan08976588098       3601PANDEGLANG                    42252         ";
            System.out.println("RESPONSE :: "+hasil);
            ParseISOMessage iSOMessage = new ParseISOMessage();
            Map<String, String> bitMapNyo = null;
        } catch (Exception e) {
            e.printStackTrace();
            
        }
        
        return hasil;
    }
    
    
    public String request_payment(String amount, String inbox_id, String bit48, String bit63) {
        String hasil = "";
        try {
            logger.info("Connecting to " + SERVER_IP + " on port " + PORT_SERVER);
            Socket client = new Socket(SERVER_IP, PORT_SERVER);
            client.setSoTimeout(this.sleepTime);
            hasil = payment(inbox_id, amount, bit48, bit63, client);
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hasil;
    }
    
    public String payment(String inbox_id, String amount, String bit48, String bit63, Socket client) {
        String hasil = "";
        try {
            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
            
            isoMsg.setMTI("2200");
            isoMsg.set(2, "99508");
            isoMsg.set(4, amount);
            isoMsg.set(11, String.format("%12s", inbox_id).replace(' ', '0'));
            isoMsg.set(12, tgl);
            isoMsg.set(26, "6021");
            isoMsg.set(32, "4510017");
            isoMsg.set(33, "4510206");
            isoMsg.set(41, "0000000000000048");

            isoMsg.set(48, bit48);
            isoMsg.set(63, bit63);
            
            byte[] datax = isoMsg.pack();
            String networkRequest = new String(datax);
            hasil = printWriter(networkRequest);
            
//            String response = "211050300041828100020599508360000000024250000000000012320190611110728602107451001707451020800000000000000000048257JTL53L30000002426690250  01OIEJ HAIJ LIEN       (PST:  1)1101  SEMARANG                                                                                            000000240000000000002500000000242500000000000000CE431880DA7F588F                20180409080812174pids16060001                    egateza                       Kp. Kadubuluh Desa Sukasari Kecamatan Kaduhejo Pan08976588098       3601PANDEGLANG                    42252         ";
            System.out.println("RESPONSE :: "+hasil);
            ParseISOMessage iSOMessage = new ParseISOMessage();
            Map<String, String> bitMapNyo = null;
        } catch (Exception e) {
            e.printStackTrace();
            
        }
        return hasil;
    }
    
    
     public static String[] getUserdata() {
        String[] userData = new String [10];
        userData[0] = "pids16060001";
        userData[1] = "egateza";
        userData[2] = "Kp. Kadubuluh Desa Sukasari Kecamatan Kaduhejo Pandeglang Banten";
        userData[3] = "08976588098";
        userData[4] = "Ega";
        userData[5] = "PANDEGLANG";
        userData[6] = "36.01";
        userData[7] = "42252";


        if (userData[1].length() > 30) {
            userData[1] = userData[1].substring(0, 30);
        }
        if (userData[2].length() > 50) {
            userData[2] = userData[2].substring(0, 50);
        }
        if (userData[3].length() > 18) {
            userData[3] = userData[3].substring(0, 18);
        }

        if (userData[5].length() > 30) {
            userData[5] = userData[5].substring(0, 10);
        }
        
        return userData;
    }
}
