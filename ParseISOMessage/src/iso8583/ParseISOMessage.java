/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package iso8583;

/**
 *
 * @author Administrator
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
//package iso8583;
/**
 *
 * @author ngonar
 */
//import PLN.SLS.NonTagList;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jpos.iso.*;

import org.jpos.iso.packager.*;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class ParseISOMessage {

//    private static Logger logger = Logger.getLogger(ParseISOMessage.class);
    public static void main(String[] args) throws IOException, ISOException {
//        GenericPackager packager = new GenericPackager("isoSLSascii.xml");
        GenericPackager packager = new GenericPackager("iso87ascii.xml");
        ParseISOMessage iSOMessage = new ParseISOMessage();
        Map<String, String> bitMapNyo = new HashMap<String, String>();

        String isoX[] = {
            //        "211050300041828100000599501360000000005459200008711148720170101081012602107451001707451020800000000000000000992232ST145S352205127032310145188175220512703239499640000000TAHUDIN                  52205123              R1000000450000000000201612201220160000000000000048592D00000000000000000000000006000004345000044530000000000000000000000000000000000",
            //        "2100403000418081000005995191710000594252017010310563160100700004510755555550000000000000000021  349        00000021"
            //        "211050300041828100000599501360000000013260500100100178120170104170459601107000015207456789900007002            23600000005110201019251010SYM21216D1177B52364A32287678125SUMARDIYANTO                                 R1  0000009000000018002011032010022000000000000000130805000000000000000000000000000000000000329550003317400000000000000000000000000000000",
            //        "211050300041828100000599501360000000008060500100100177720161207170459601107000015207456789900007002            23200000005110201013471010SYM2121626542037836351881628168RIDUANSYAH                                   R1  000000900000001800201103201002200000000000000078805000000000000000000000000000000000084050000856900000000000000000000000000000000"
            //        "211050300041828100000599509360000000005960000100100204120170110142324601107000015207152300600007001            124  253     00207900760201612201612R.SUKARYA                     0101000000882646952016120000000568000000000000001837-00001850"
            //        "211050300041828100000599501360000000008208000000010779420161221113810602107451001707456789800000000000000000048581ST145S35110201102814040SYM2121694828C890875DB77C7D20B7ANDI HARUN RASYID HA     51102022-1234567    R1  0000009000000000002010122010012200000000000000021420000000000000000000000000000000000000301760003018200000000000000000000000000000000201102201001220000000000000002142000000000000000000000000000000000000030176000301820000000000000000000000000000000020110320100220000000000000000196200000000000000000000000000000000000003018200030182000000000000000000000000000000002011032010022000000000000000019620000000000000000000000000000000000000301820003018200000000000000000000000000000000"
            //        "22105032004182810002059950136000000002336900010010024772017012513504220170125601107000015207456789800007001            352000000051102029389522020SYM2121659C27D548080024475B3802SUBAHRI                                      R1  00000090000000000020110320100122000000000000001360000000000000000000000000000000000000000920400009474000000000000000000000000000000002011032010022000000000000000097690000000000000000000000000000000000000094740000967300000000000000000000000000000000080Rincian Tagihan dapat diakses di www.pln.co.id,Informasi Hubungi Call Center:123"
            //        "22105032004182810002059950136000000000121100010010024522017012513504220170125601107000015207456789900007001            352000000051102029389522020SYM2121659C27D548080024475B3802SUBAHRI                                      R1  00000090000000000020110320100122000000000000001360000000000000000000000000000000000000000920400009474000000000000000000000000000000002011032010022000000000000000097690000000000000000000000000000000000000094740000967300000000000000000000000000000000080Rincian Tagihan dapat diakses di www.pln.co.id,Informasi Hubungi Call Center:123"
            //        "221050300041828100000599501360000000000000000100100245320170125135042601107000015207456789900307001            352000000051102029389522020SYM2121659C27D548080024475B3802SUBAHRI                                      R1  00000090000000000020110320100122000000000000001360000000000000000000000000000000000000000920400009474000000000000000000000000000000002011032010022000000000000000097690000000000000000000000000000000000000094740000967300000000000000000000000000000000"
            //        "22105032004182810002059950136000000000535000010010024542017012513504220170125601107000015207456789900007001            352000000051102029389522020SYM2121659C27D548080024475B3802SUBAHRI                                      R1  00000090000000000020110320100122000000000000001360000000000000000000000000000000000000000920400009474000000000000000000000000000000002011032010022000000000000000097690000000000000000000000000000000000000094740000967300000000000000000000000000000000080Rincian Tagihan dapat diakses di www.pln.co.id,Informasi Hubungi Call Center:123",
            //        "22105032004182810002059950136000000001543750010010008532016112617590520161126601107000015207456789800007002            344000000051102021422322020SYM21216673660703A322631677C523AKHMAD ILYAS H                               R1  000000900000000000201102201001220000000000000085280000000000000000000000000000000000075340000771000000000000000000000000000000000201103201002200000000000000069095000000000000000000000000000000000077100000785600000000000000000000000000000000080Rincian Tagihan dapat diakses di www.pln.co.id,Informasi Hubungi Call Center:123"
            //        "21004030004180810000059950100000000000120170127160008602107451001707451021600000000000000010190000000511020304056"};
            //  "2210503200418281000205995013600000000005288000000000000201707051042222017070560100700001520715230060000171             233000000053563243280511010BAG210ZE68EBA6D10620E66D7B9DD0D                    UKARA                 123  R1000000450000000000201707                00000005288000000000000000000000000000000000975000009770000000000000000000000000000000000080Rincian Tagihan dapat diakses di www.pln.co.id,Informasi Hubungi Call Center:123"
            //        "2110503000418281000005995083600000000000000000115380501201707180827386012079888300070000000000006852027SLS00010000001318636721  01?"
//            "21104030004182810000059950100000000026220170906110156601307451001707BULK00100900000000000000048019MITRA01541100323334"
//            "0210723A00010AC1800806064009170000000000540750122107274285085607274012211222030080001468508561253PLG001201211111111111166000105322067561   UBAIDILLAH MA&#039;RUF        D3FA94EC6E0703C2EB97D57BF20F8DBA10000300000000000712A       000000537750           000000000000           000000000000360001"

//            "21104030004182810000059950200014692590420171221145353602107000000007506227600150000000000003346031VI105V3860012345370000000000000"
//              "0210FA3A00014E019400000000000600000002008200000000004688050000004688050221084233000998084228022102220304000000000000932      00171020090000000162002287775329  030006RONA MUTIARA                  0000000000000000000000000000000000002802A       000000393608801A       000000075197           00000000000036000000000000000000000000'"
//                "221050300041828100040599502360000000002160000015875936520180308120436602107000000007506227600630000000000006852145506CA01565042686755205918543950000000000000000000000000000000001E0E412BAD464E48A898BCF70A46F968KAMDI                    R1  0000013002000016000000285252059123            009360"
//                "22105032004182810006059950236000000002000000000002193052018062809514320180629602107451001707456789800000000000000000048241451CA0101428800711534100000023143537603929I943288520129E74487E70SYM21SB2164757589457957684D57243919571IDRS S'AMIRI, SH            R300000130002000000000020000000000200000000002000015000020000500000200001935000020000031983499417635401862424530395351128022-1234567    00120200000200000064Rincian tagihan dapat diakses di www.pln.co.id atau PLN terdekat"
//                "22105032004182810006059950236020000000200000001666751682018070411184120180705602107451001707451021600000000000000000048241SWID003321175333592310219192640                                042621CA4435623D3E298547D1054953        KHOIRI EFFENDI           R1       13000200000000002000000000020000000000200001819002000000000020000018181002000000124058101798650650790794039                   12300000000000000000059Informasi Hubungi Call Center 123 Atau hubungi PLN Terdekat"
//                "0210723A00010AC18000060640093800000000009768330815110915228518110915081508150300800000022851800DEVELPLG2009001008000000880001028425625883  Pelanggan 754                 1000025000000000000000000000000000976833360"
//                "0210723A00010AC1800806064009170000000000333800122013490473020613485312201221030080001867302060053PLG00120121111111111118000010111812100142 DUWI IHTIARDI                 8BD9DD308532915DF3F6A60E41D1DD5510000300000000000812A       000000330800           000000000000           00000000000020181220134911360001 \\x03"
//                "22105032004182810006059950236000000000200000010013174902019011212445620190112601107000015207152300600007013            2410000000567098346775471043335521000000000000000000000000000000000RTL213519401E90EABDDFEF8550DB7600000000WASUDI (L)               R1M 00000090000000000000020000000.0020000000.0020000469.00000000000002000019531.0020000014.50178489624171676513680285454710123            000000060Informasi Hubungi Call Center 123 Atau hubungi PLN Terdekat"
//                "22005030004180810004059950236000000000500000000002509682019011717465660210745100170745678980000000000000048145451CA01014288007115341000000230501689895517484983I03858O02457200SYM21SB21639D7571D1B9342E90049DDRS S'AMIRI, SH            R30000013002000000000000505351128022-1234567    0012020000002000000000050000000"
//                "0210723A40010AC18002061340021700000000052025000131143249347984143249013102016012030080001953479840153PLG0012012111111111112048888801463970734  01SAHAL ARIFIN         (PST:  5)                                                                                                          000005200000000000002500000005202500000000000000360134HTH18090021                     PT Shopee International IndonePacific Century Place (PCP) Tower SCBD, Lot 10, 2608118697520       3174"
//                  "21004030004180810000059950200000024975520190110232057602107451001707456789800000000000000480310000000014288007110000000000000"
//            "22005030004180810004059950236000000000525000001988989432019022705193760210700000000750622790000000000010031145506CA0156801705676537116516156100000000000000000000000000000000AC85CC02884344FABF4732E3EB2AB03EDIAN MARYANI LUBIS       R1  0000013002000025000000285353711123            009360"
//            "223150300041828100040599502360000000005000057134363291520190223175706602107451001707451101500630000000000000001145451CA011425673456855130010654702D4B9D3EC9CD4D37933FA7A707A2A5860SYM21SB018459362886659509133571NI MADE MUDRI            R1M 0000009002000000000000285555021123            006480ÿ"
//                "2110503000418281000005995010000000000000000000000683993201903241027126021074510017074511026008800000000000041960190000000147700109718ÿ"
//                "22005030004180810004059950236000000000225000002039237672019032811501160210700000000750622790000000000006852145506CA0145023563047524510882504000000000000000000000000000000000FBC14501E1204E09A7B0BB6B2EE077DEWARNO IPUNG              R1M 0000009002000025000000285252451123            006480"
//                "21104030004182810004059950272284006811520190330173018602107451001707451101500000000000000000001144451CA01143349327965245107390951EC7F642CC8AD45A8BD9FDC7A1EC60E530SYM21SB018798B02021368674240776ALIF BAHTIAR             R1  000000900200000000000285252451123            006480ÿ"
//                "221050300041828100040599502360000000010250000020545152620190410092214602107000000007506227900510000000000009947145506CA01565050046085245414456180000000000000000000000000000000002DBDD8BE14B3457B8A3FB2AA60743E38NASRUDIN                 R1  0000022002000025000000285252454123            015840"
//                "0210623A400182C180080609401038000004091207532312212019040409041060210300807456789800DEVELPLG2009001008000001361121202210239   16USEEKIDDYH          INDIKIDS                                          00000000000010USEEINMV1H          INDIMOVIE 1                                       00000007500011USEEINMV2H          INDIMOVIE 2                                       00000007000000USEEINHBLH          INDIMOVIE LITE                                    00000000000000USEEINDS1H          DYNASTY 1                                         00000000000000USEEINDS2H          DYNASTY 2                                         00000000000000USEEINNWHD          INDINEWS                                          00000000000000USEEINT1HD          INDITAINMENT 1                                    00000000000000USEEINT2HD          INDITAINMENT 2                                    00000000000000USEEINKRHD          KARAOKE ON DEMAND 1                               00000000000000SVOD101             SVOD101                                           00000000000000USEEINDYHD          INDIKIDS LITE                                     00000000000000USEEBEINHD          INDISPORT                                         00000000000010USEEINBASH          INDIBASKETBALL                                    00000000000000USEEINGLFH          INDIGOLF                                          00000000000000USEECOMB6H          BASKET-GOLF-INDITAINMENT LITE                     000000000000003603360217199608     122218201962   122218201962 Internet Speed 10 M Kuota 50 Gb Zona 3            KEMANG UTARA E 8 008/01 BANGKA JAKARTA SELATAN 12730                                                SARINI BIN HASBIH             081213437033 sya.roel@gmail.com                                                                                  "
                "211050300041828100020599508360000000024250000000026420220190610164726601207451001707451020800000000000000000048257JTL53L30000002426690250  01OIEJ HAIJ LIEN       (PST:  1)1101  SEMARANG                                                                                            000000240000000000002500000000242500000000000000CE431880DA7F588F                20180409080812174plg12110004                     Myelectricreload              Margahayu Jaya blok A no 165, Jln cemara no.6     1234567           3275BEKASI                        0000017113"
            
        };
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

        for (int i = 0; i < isoX.length; i++) {
            try {
                System.out.println(String.format("%200s", " ").replace(" ", "="));
                System.out.println(isoX[i]);
                System.out.println(String.format("%200s", " ").replace(" ", "="));

                System.out.println(isoX[i].length());
                bitMapNyo = iSOMessage.getBitmap(isoX[i]);

                if (bitMapNyo.get("MTI").equalsIgnoreCase("2100") && bitMapNyo.get("2").contains("99502")) {
                    System.out.println("inquiry request");
                    String[] _48 = iSOMessage.inquiryRequestPrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                } else if (bitMapNyo.get("MTI").equalsIgnoreCase("2110") && bitMapNyo.get("2").contains("99502")) {
                    System.out.println("inquiry response");
                    String[] _48 = iSOMessage.inquiryResponsePrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                } else if (bitMapNyo.get("MTI").equalsIgnoreCase("2200")
                        && bitMapNyo.get("MTI").equalsIgnoreCase("2220")
                        && bitMapNyo.get("MTI").equalsIgnoreCase("2221")
                        && bitMapNyo.get("2").contains("99502")) {
                    String[] _48 = iSOMessage.paymentRequestPrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                }//parsing payment termasuk advice 
                else if (bitMapNyo.get("MTI").equalsIgnoreCase("2200") && bitMapNyo.get("2").contains("99502")) {
                    String[] _48 = iSOMessage.paymentRequestPrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                } else if (bitMapNyo.get("MTI").equalsIgnoreCase("2210") && bitMapNyo.get("2").contains("99502")) {
                    if (!bitMapNyo.get("39").equalsIgnoreCase("0000")) {
                        String[] _48 = iSOMessage.paymentRequestPrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                    } else {
                        String[] _48 = iSOMessage.paymentResponsePrepaidUnpackBit48(bitMapNyo.get("48"), "0000");
                    }
                }

                if (bitMapNyo.get("MTI").equalsIgnoreCase("2100") && bitMapNyo.get("2").contains("99501")) {
                    String[] _48 = iSOMessage.inquiryRequestPostpaidUnpackBit48(bitMapNyo.get("48"), "0000");
                } else if (bitMapNyo.get("MTI").equalsIgnoreCase("2110") && bitMapNyo.get("2").contains("99501")) {
                    String[] _48 = iSOMessage.inquiryResponsePostpaidUnpackBit48(bitMapNyo.get("48"), "0000");
                } else if ((bitMapNyo.get("MTI").equalsIgnoreCase("2200")
                        || bitMapNyo.get("MTI").equalsIgnoreCase("2400")
                        || bitMapNyo.get("MTI").equalsIgnoreCase("2401"))
                        && bitMapNyo.get("2").contains("99501")) {
                    String[] _48 = iSOMessage.paymentRequestPostpaidUnpackBit48(bitMapNyo.get("48"), "0000");
                }//parsing payment termasuk advice 
                else if (bitMapNyo.get("2").contains("99501")) {
                    if (!bitMapNyo.get("39").equalsIgnoreCase("0000")) {
                        String[] _48 = iSOMessage.paymentRequestPostpaidUnpackBit48(bitMapNyo.get("48"), "0000");
                    } else {
                        String[] _48 = iSOMessage.paymentResponsePostpaidUnpackBit48(bitMapNyo.get("48"), "0000");
                    }
                }

                if (bitMapNyo.get("MTI").equalsIgnoreCase("2100") && bitMapNyo.get("2").contains("99504")) {
                    String[] _48 = iSOMessage.inquiryRequestNontagUnpackBit48(bitMapNyo.get("48"));
                } else if (bitMapNyo.get("MTI").equalsIgnoreCase("2110") && bitMapNyo.get("2").contains("99504")) {
                    String[] _48 = iSOMessage.inquiryResponseNontagUnpackBit48(bitMapNyo.get("48"));
                } else if ((bitMapNyo.get("MTI").equalsIgnoreCase("2200")
                        || bitMapNyo.get("MTI").equalsIgnoreCase("2400")
                        || bitMapNyo.get("MTI").equalsIgnoreCase("2401"))
                        && bitMapNyo.get("2").contains("99504")) {
                    String[] _48 = iSOMessage.paymentRequestNontagUnpackBit48(bitMapNyo.get("48"));
                }//parsing payment termasuk advice 
                else if (bitMapNyo.get("2").contains("99504")) {
                    if (!bitMapNyo.get("39").equalsIgnoreCase("0000")) {
                        String[] _48 = iSOMessage.paymentRequestNontagUnpackBit48(bitMapNyo.get("48"));
                    } else {
                        String[] _48 = iSOMessage.paymentResponseNontagUnpackBit48(bitMapNyo.get("48"));
                    }
                }

//                String axz = "JTL53L314234567891551111111111052CB5394776D344FD05A9CF624B3DCCB02EA00C4C6FDE61371137B5B59106A92BUDIM\"AN/.,ST            R1  000001300200000000000";
//                axz = bitMapNyo.get("48");
//                String tokenType = axz.substring((axz.length() - 1));                 //new token not unsold
//                System.out.println("token type " + tokenType + "_48 length = " + axz.length());
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

//        String[] _48 = iSOMessage.inquiryUnpackBit48(bitMapNyo.get("48"), "0000");
    }

    public static String[] inquiryResponseNontagUnpackBit48(String msg) {
        String[] hasil = new String[65];
        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc id",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                //System.out.println(" l : "+l+", f : "+f);
                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return hasil;
    }

    public static String[] inquiryRequestNontagUnpackBit48(String msg) {
        String[] hasil = new String[65];
        int[] seq = {7, 13, 3};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",};
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                //System.out.println(" l : "+l+", f : "+f);
                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return hasil;
    }

    public static String[] paymentRequestNontagUnpackBit48(String msg) {
        String[] hasil = new String[65];
        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc id",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                //System.out.println(" l : "+l+", f : "+f);
                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }
        return hasil;
    }

    public static String[] paymentResponseNontagUnpackBit48(String msg) {
        String[] hasil = new String[65];

        int[] seq = {7, 13, 3, 25, 8, 8, 12, 25, 32, 32, 5, 35, 15, 1, 17, 1, 17, 1, 10};
        String[] title = {
            "switcher id",
            "reg no.",
            "trx code",
            "trx name",
            "reg date",
            "exp date",
            "subsc id",
            "subsc name",
            "PLN Reference Number",
            "SLS Receipt reference",
            "Service Unit",
            "Service Unit Address",
            "Service Unit Phone",
            "Total transaction amount minor unit",
            "Total transaction amount",
            "PLN bill minor unit",
            "PLN bill",
            "Adm Charge minor unit",
            "Adm Charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (seq[i] == 1) {
                    hasil[i] = String.valueOf(msg.charAt(l));
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    l += seq[i];
                } else if (seq[i] > 1) {
                    f = n;
                    l = n + seq[i];
                    hasil[i] = msg.substring(f, l);
                    System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                }
                n = l;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] inquiryRequestPostpaidUnpackBit48(String msg, String rc) {
        String[] hasil = new String[65];

//        int[] seq1 = {7,12,1,2,32,25,5,15,4,9,9,
//                     6,8,8,11,11,10,9,8,8,8,8,8,8                     
//                    };
//        asjdhfjkahsdlfkjhalsjkdhfjklahsdfkh
        int[] seq1 = {7, 12};

        int[] seq2 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq3 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq4 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq = null;

        seq = seq1;

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] inquiryResponsePostpaidUnpackBit48(String msg, String rc) {
        String[] hasil = new String[65];

        int[] seq1 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq2 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq3 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq4 = {7, 12, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
        };

        int[] seq = null;

        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1")) {
            seq = seq1;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("2")) {
            seq = seq2;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("3")) {
            seq = seq3;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("4")) {
            seq = seq4;
        }

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] paymentRequestPostpaidUnpackBit48(String msg, String rc) {
        String[] hasil = new String[65];

//        int[] seq1 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq2 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq3 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq4 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
//        };
        int[] seq1 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
        };

        int[] seq2 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
        };

        int[] seq3 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,};

        int[] seq4 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
        };

        int[] seq = null;

        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1")) {
            seq = seq1;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("2")) {
            seq = seq2;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("3")) {
            seq = seq3;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("4")) {
            seq = seq4;
        }

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "payment status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

    public static String[] paymentResponsePostpaidUnpackBit48(String msg, String rc) {
        String[] hasil = new String[65];

                int[] seq1 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
                };
        
                int[] seq2 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
                };
        
                int[] seq3 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
                };
        
                int[] seq4 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8,
                    6, 8, 8, 11, 11, 10, 9, 8, 8, 8, 8, 8, 8
                };
//        int[] seq1 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq2 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq3 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
//        };
//
//        int[] seq4 = {7, 12, 1, 1, 2, 32, 25, 5, 15, 4, 9, 9,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8,
//            6, 8, 8, 12, 11, 10, 12, 8, 8, 8, 8, 8, 8
//        };

        int[] seq = null;

        if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("1")) {
            seq = seq1;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("2")) {
            seq = seq2;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("3")) {
            seq = seq3;
        } else if (String.valueOf(msg.charAt(19)).equalsIgnoreCase("4")) {
            seq = seq4;
        }

        String[] title = {
            "switcher id",
            "subscriber id",
            "bill status",
            "Payment status",
            "total outstanding bill",
            "switcher refnum",
            "subscriber name",
            "service unit",
            "service unit phone",
            "subscriber segmentation",
            "power consuming category",
            "total admin charges",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3",
            "bill period", "due date", "meter read date", "total electricity bill", "incentive", "vat", "penalty fee", "prev meter reading 1",
            "curr meter reading 1", "prev meter reading 2", "curr meter reading 2", "prev meter reading 3", "curr meter reading 3"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
                        System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }

//////////////////////////////////prepaid///////////////////////////////////////////    
    public String[] inquiryRequestPrepaidUnpackBit48(String msg, String rc) {
        System.out.println("bit48 : " + msg);
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
//        String[][] hasil = new String[2][65];

//        int[] main = {15, 12, 30, 2, 2, 12, 8};
//        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9};
        int[] main = {7, 11, 12, 1};
        int[] detal = {6, 12, 8, 17};
        int[] seq = null;
        int indexData = 0;

        String[] titleMain = {
            //            "length",
            "switcher_id", //switcher id
            "meter_serial_number", //meter id
            "subscriber_id", //idpel
            "flag"
//                , //flag
//            "switcher_ref_no", //trx_id
//            "pln_ref_no", //ref_id
//            "Subscriber_name", //nama
//            "subscriber_segmentation", //tarif
//            "Power_consuming_cat", //kategori_daya
        };
        String[] slsBit48Index = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_ref_no",
            "switcher_ref_no",
            "Subscriber_name",
            "subscriber_segmentation",
            "Power_consuming_cat",
            "minor_unit_admin_charge",
            "admin_charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return hasil;
    }

    public String[] inquiryResponsePrepaidUnpackBit48(String msg, String rc) {
        System.out.println("bit48 : " + msg);
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
//        String[][] hasil = new String[2][65];

//        int[] main = {15, 12, 30, 2, 2, 12, 8};
        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9, 1, 10};
//        int[] main = {7, 11, 12, 1};
        int[] detal = {6, 12, 8, 17};
        int[] seq = null;
        int indexData = 0;

        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_ref_no",
            "switcher_ref_no",
            "Subscriber_name",
            "subscriber_segmentation",
            "Power_consuming_cat",
            "minor_unit_admin_charge",
            "admin_charge"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return hasilX;
    }

    public String[] paymentRequestPrepaidUnpackBit48(String msg, String rc) {
        System.out.println("bit48 : " + msg);
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
//        String[][] hasil = new String[2][65];

//        int[] main = {15, 12, 30, 2, 2, 12, 8};
        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9, 1, 10, 1};
//        int[] main = {7, 11, 12, 1};
        int[] detal = {6, 12, 8, 17};
        int[] seq = null;
        int indexData = 0;

        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_ref_no",
            "switcher_ref_no",
            "Subscriber_name",
            "subscriber_segmentation",
            "Power_consuming_cat",
            "minor_unit_admin_charge",
            "admin_charge",
            "buying option"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return hasilX;
    }

    public String[] paymentResponsePrepaidUnpackBit48(String msg, String rc) {
        System.out.println("bit48 : " + msg);
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
//        String[][] hasil = new String[2][65];

//        int[] main = {15, 12, 30, 2, 2, 12, 8};
        int[] main = {7, 11, 12, 1, 32, 32, 8, 25, 4, 9, 1, 1, 10, 1, 10, 1, 10, 1, 10, 1, 10, 1, 12, 1, 10, 20};
//        int[] main = {7, 11, 12, 1};
        int[] detal = {6, 12, 8, 17};
        int[] seq = null;
        int indexData = 0;

        String[] titleMain = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_ref_no",
            "switcher_ref_no",
            "vending no",
            "Subscriber_name",
            "subscriber_segmentation",
            "Power_consuming_cat",
            "buying option",
            "minor_unit_admin_charge",
            "admin_charge",
            "minor stamp",
            "stamp",
            "minor ppn",
            "ppn",
            "minor ppj",
            "ppj",
            "minor installment",
            "intallment",
            "minor power purchase",
            "power purchase",
            "minor purchase kwh",
            "purchase kwh",
            "token"
        };

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
//                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return hasilX;
    }

//f 
    public String[] inquiryUnpackBit48(String msg, String rc) {
        System.out.println("bit48 : " + msg);
        String[] hasil = new String[65];
        String[] hasilX = new String[12];
//        String[][] hasil = new String[2][65];

//        int[] main = {15, 12, 30, 2, 2, 12, 8};
        int[] main = {7, 11, 12, 1, 32, 32, 25, 4, 9};
        int[] detal = {6, 12, 8, 17};
        int[] seq = null;
        int indexData = 0;

//        String[] titleMain = {
//            "switcher_id", 
//            "meter_id", 
//            "idpel", 
//            "flag", 
//            "trx_id",
//            "ref_id", 
//            "nama", 
//            "tarif", 
//            "kategori_daya"
//        };
        String[] titleMain = {
            "switcher_id", //switcher id
            "meter_serial_number", //meter id
            "subscriber_id", //idpel
            "flag", //flag
            "switcher_ref_no", //trx_id
            "pln_ref_no", //ref_id
            "Subscriber_name", //nama
            "subscriber_segmentation", //tarif
            "Power_consuming_cat", //kategori_daya
        };
        String[] slsBit48Index = {
            "switcher_id",
            "meter_serial_number",
            "subscriber_id",
            "flag",
            "pln_ref_no",
            "switcher_ref_no",
            "Subscriber_name",
            "subscriber_segmentation",
            "Power_consuming_cat",
            "minor_unit_admin_charge",
            "admin_charge"
        };

        //note: inisialisasi, jadi kalau ada data yang kurang dari jpa maka 
        //tidak masuk ke dalam exception
        Map<String, String> bit48map = new HashMap<String, String>();
        bit48map.put("switcher_id", "");
        bit48map.put("meter_serial_number", "");
        bit48map.put("subscriber_id", "");
        bit48map.put("flag", "");
        bit48map.put("pln_ref_no", "");
        bit48map.put("switcher_ref_no", "");
        bit48map.put("Subscriber_name", "");
        bit48map.put("subscriber_segmentation", "");
        bit48map.put("Power_consuming_cat", "");
        bit48map.put("minor_unit_admin_charge", "2");
        bit48map.put("admin_charge", "0000160000");

        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < main.length; i++) {
                indexData = i;
                hasil[indexData] = "";

                if (rc.equalsIgnoreCase("0000")) {
                    if (main[i] == 1) {
                        hasil[indexData] = String.valueOf(msg.charAt(l));
                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += main[i];
                    } else if (main[i] > 1) {
                        f = n;
                        l = n + main[i];
                        hasil[indexData] = msg.substring(f, l);
                        bit48map.put(titleMain[i], hasil[indexData]);
                        System.out.println(String.format("%-50s", titleMain[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                n = l;
            }

            for (int i = 0; i < slsBit48Index.length; i++) {
                hasilX[i] = bit48map.get(slsBit48Index[i]);
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return hasilX;
    }

    /*
     public static void main(String[] args) throws IOException, ISOException {
     // Create Packager based on XML that contain DE type
     GenericPackager packager = new GenericPackager("D:/Pelangi/PELANGI-INTAKA/PelangiClientPC/isoSLSascii.xml");
     //GenericPackager packager = new GenericPackager("iso87ascii.xml");

     // Print Input Data
     //String data = "0200B2200000001000000000000000800000201234000000010000011072218012345606A5DFGR021ABCDEFGHIJ 1234567890";
     //String data = "2800001000008181000020091014011452074410101001SLS0000000000001007ST145S3";
     //String data = "21004030004180810000059950200000014338820110201065634601207000000007009010100JTM1504ZMSDY060310000000011121503780000000000000";
     //String data = "210040300041808100000599504000000000006200910140111366015074410000074410101SLS0000000000001044ST145S35436000000016710 54001";

     //response inquiry SLS
     //String data = "211050300041828100000599501360000000005594500000000000220120628151332601007451001707451010300000000000000000002343000000051102010130520284995624628644671172477387768199AMIR MAHMUD              51102022-1234567    R1  000000450000003200201104201002200000000000000016025000000000000000000000000000000000112470001129000000000000000000000000000000000201105201001220000000000000036720000000000000000000000000000000000111610001124700000000000000000000000000000000";

     //String data = "2210503200418281000005995013600000000095620000000025253201207051849282012070660100745100170745101030000000000000000000223300000005110202029381101907125991891045170641707189045U9MARLINA DEWI             51102022-1234567    R1  000002200000001600201105201002200000000000000094020000000000000000000000000000000000276460002769300000000000000000000000000000000";

     //1987 - net req
     //String data = "0800823A0000000000000400000000000000042009061390000109061304200420001";

     //1987 - net response
     //String data = "0810823A000002000000048000000000000004200906139000010906130420042000001031128";

     //1987 - trx req
     //String data = "0200323A40010841801038000000000000000004200508050113921208050420042251320720000010000001156040800411        01251146333156336000299 ";



     //1987 - rev req
     //                String data = "0410F23A40010A4182020000004000000000191111111110000000000180000000000030000"+
     //                "090806465100331613451909080908601006000200000000000343000003948"+
     //                "0380811001200000628110012000004096565733236003000331700039480908064651000000"+
     //                "0003132020000331609080645190000000020000000000000";

     //rev response
     //                String data = "0400F23A40010841820200000040000000001911111111100000000001800000000000300000"+
     //                                "908064651003316134519090809096010060002000000000003430003948"+
     //                                "0380811001200000409656573320000000300000136003000331700039480908064651000000"+
     //                                "0003132020000331609080645190000000020000000000000";

     //1987 - rev rep req
     //                String data = "0401F23A40010841820200000040000000001911111111100000000001800000000000300000"+
     //                "908064652003316134519090809096010060002000000000003430003948"+
     //                "0380811001200000409656573320000000300000136003000331800039480908064652000000"+
     //                "0003132020000331609080645190000000020000000000000";

     String data = "0410F23A40010A4182020000004000000000191111111110000000000180000000000030000"
     + "090806465200331613451909080908601006000200000000000343940003948        "
     + "0380811001200000409656573320000000300000136003000331800039480908064652000000"
     + "0003132020000331609080645190000000020000000000000 ";

     data = "211040300041828100000599502000001171338201207251614206010074510017074510103003300000000000000020310000000000000000000000000000000";

     System.out.println("DATA : " + data);

     Map<String, String> bitMapNyo = new HashMap<String, String>();
     bitMapNyo = getBitmap(data);

     //System.out.println(String.format("%10s", "foo").replace(' ', '0'));

     // Create ISO Message
     ISOMsg isoMsg = new ISOMsg();
     isoMsg.setPackager(packager);
     isoMsg.unpack(data.getBytes());

     String fieldDescription = packager.getFieldDescription(isoMsg, 2);
     System.out.println("desctiption : " + fieldDescription);
     // print the DE list
     logISOMsg(isoMsg, packager);
     }

     *
     *
     */
    private static void logISOMsg(ISOMsg msg, GenericPackager packager) {
        System.out.println("----ISO MESSAGE-----");
        try {
            /*	System.out.println("  MTI : " + msg.getMTI());
             for (int i=1;i<=msg.getMaxField();i++) {
             if (msg.hasField(i)) {
             System.out.println("    Field-"+i+" : "+msg.getString(i));
             }
             }
             *
             */
            System.out.println("  MTI : " + msg.getMTI());

            System.out.println("  Bitmap : " + msg.getValue(-1).toString());

            String bitmapString = msg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] //bitmapArray = { msg.getMTI() };
                    bitmapArray = bitmapString.split(",");

            for (int i = 0; i < bitmapArray.length; i++) {
                String string = bitmapArray[i].trim();
                System.out.println("bitmap " + i + " : " + packager.getFieldDescription(msg, Integer.parseInt(string)) + string.trim());
            }
            System.out.println("=====================================");

            System.out.println("  2 : " + msg.getString(2));
            System.out.println("  3 : " + msg.getString(3));
            System.out.println("  4 : " + msg.getString(4));
            System.out.println("  5 : " + msg.getString(5));
            System.out.println("  6 : " + msg.getString(6));
            System.out.println("  7 : " + msg.getString(7));
            System.out.println("  11 : " + msg.getString(11));
            System.out.println("  12 : " + msg.getString(12));
            System.out.println("  13 : " + msg.getString(13));
            System.out.println("  15 : " + msg.getString(15));
            System.out.println("  18 : " + msg.getString(18));
            System.out.println("  26 : " + msg.getString(26));
            System.out.println("  32 : " + msg.getString(32));
            System.out.println("  33 : " + msg.getString(33));
            System.out.println("  37 : " + msg.getString(37));
            System.out.println("  39 : " + msg.getString(39));
            System.out.println("  40 : " + msg.getString(40));
            System.out.println("  41 : " + msg.getString(41));
            System.out.println("  42 : " + msg.getString(42));
            System.out.println("  48 : " + msg.getString(48));
            System.out.println("  49 : " + msg.getString(49));
            System.out.println("  55 : " + msg.getString(55));
            System.out.println("  60 : " + msg.getString(60));
            System.out.println("  63 : " + msg.getString(63));
            System.out.println("  70 : " + msg.getString(70));
            System.out.println("  73 : " + msg.getString(73));
            System.out.println("  90 : " + msg.getString(90));

            // String[] iwan = parseBit48Inquiry(msg.getString(48));
            //System.out.println("iwan : "+iwan[3]);
        } catch (ISOException e) {
            e.printStackTrace();
        } finally {
            System.out.println("--------------------");
        }

    }

    public static String[] parseBit48Inquiry(String msg) {
        String[] hasil = new String[65];

        try {
            hasil[0] = msg.substring(0, 7);
            System.out.println("switcher id : " + hasil[0]);// switcher id - 7
            hasil[1] = msg.substring(7, 19);
            System.out.println("sub id : " + hasil[1]);// subscriber id - 12
            hasil[2] = String.valueOf(msg.charAt(19));
            System.out.println("bill status : " + hasil[2]); //bill status - 1
            hasil[3] = msg.substring(20, 22);
            System.out.println("outstanding bill : " + hasil[3]); //outstanding bill - 2
            hasil[4] = msg.substring(22, 54);
            System.out.println("switcher ref : " + hasil[4]); //switcher ref no - 32
            hasil[5] = msg.substring(54, 79);
            System.out.println("sub name : " + hasil[5]); //Subscriber name - 25
            hasil[6] = msg.substring(79, 84);
            System.out.println("service unit : " + hasil[6]); //Service unit - 5
            hasil[7] = msg.substring(84, 99);
            System.out.println("service unit phone : " + hasil[7]); //Service unit phone - 15
            hasil[8] = msg.substring(99, 103);
            System.out.println("sub segmentation : " + hasil[8]); //subscriber segmentation - 4
            hasil[9] = msg.substring(103, 112);
            System.out.println("power consuming cat : " + hasil[9]);  //Power consuming cat - 9
            hasil[10] = msg.substring(112, 121);
            System.out.println("total admin charge : " + hasil[10]); //Total admin charges - 9

            hasil[11] = msg.substring(121, 127);
            System.out.println("bill period : " + hasil[11]); //bill period - 6
            hasil[12] = msg.substring(127, 135);
            System.out.println("due date : " + hasil[12]); //due date - 8
            hasil[13] = msg.substring(135, 143);
            System.out.println("meter read date : " + hasil[13]); //meter read date - 8
            hasil[14] = msg.substring(143, 154);
            System.out.println("total electricity bill : " + hasil[14]); //total electricity bill - 11
            hasil[15] = msg.substring(154, 165);
            System.out.println("incentive : " + hasil[15]); //incentive - 11
            hasil[16] = msg.substring(165, 175);
            System.out.println("vat : " + hasil[16]); //vat - 10
            hasil[17] = msg.substring(175, 184);
            System.out.println("penalty fee : " + hasil[17]); //penalty fee - 9
            hasil[18] = msg.substring(184, 192);
            System.out.println("prev meter reading : " + hasil[18]); //prev meter reading 1 - 8
            hasil[19] = msg.substring(192, 200);
            System.out.println("curr meter reading : " + hasil[19]); //curr meter reading 1 - 8
            hasil[20] = msg.substring(200, 208);
            System.out.println("prev meter reading 2 : " + hasil[20]); //prev meter reading 2 - 8
            hasil[21] = msg.substring(208, 216);
            System.out.println("curr meter reading 2 : " + hasil[21]); //curr meter reading 2 - 8
            hasil[22] = msg.substring(216, 224);
            System.out.println("prev meter reading 3 : " + hasil[22]); //prev meter reading 3 - 8
            hasil[23] = msg.substring(224, 232);
            System.out.println("curr meter reading 3 : " + hasil[23]); //curr meter reading 3 - 8

            if (Integer.parseInt(hasil[2]) > 1) {
                hasil[24] = msg.substring(232, 238); //bill period - 6
                hasil[25] = msg.substring(238, 246); //due date - 8
                hasil[26] = msg.substring(246, 254); //meter read date - 8
                hasil[27] = msg.substring(254, 265); //total electricity bill - 11
                hasil[28] = msg.substring(265, 276); //incentive - 11
                hasil[29] = msg.substring(276, 286); //vat - 10
                hasil[30] = msg.substring(286, 295); //penalty fee - 9
                hasil[31] = msg.substring(295, 303); //prev meter reading 1 - 8
                hasil[32] = msg.substring(303, 311); //curr meter reading 1 - 8
                hasil[33] = msg.substring(311, 319); //prev meter reading 2 - 8
                hasil[34] = msg.substring(319, 327); //prev meter reading 2 - 8
                hasil[35] = msg.substring(327, 335); //prev meter reading 3 - 8
                hasil[36] = msg.substring(335, 343); //prev meter reading 3 - 8
            }

            if (Integer.parseInt(hasil[2]) > 2) {
                hasil[37] = msg.substring(343, 349);
                hasil[38] = msg.substring(349, 357);
                hasil[39] = msg.substring(357, 365);
                hasil[40] = msg.substring(365, 376);
                hasil[41] = msg.substring(376, 387);
                hasil[42] = msg.substring(387, 397);
                hasil[43] = msg.substring(397, 406);
                hasil[44] = msg.substring(406, 414);
                hasil[45] = msg.substring(414, 422);
                hasil[46] = msg.substring(422, 430);
                hasil[47] = msg.substring(430, 438);
                hasil[48] = msg.substring(438, 446);
                hasil[49] = msg.substring(446, 454);
            }

            if (Integer.parseInt(hasil[2]) > 3) {
                hasil[50] = msg.substring(454, 460);
                hasil[51] = msg.substring(460, 468);
                hasil[52] = msg.substring(468, 476);
                hasil[53] = msg.substring(476, 487);
                hasil[54] = msg.substring(487, 498);
                hasil[55] = msg.substring(498, 508);
                hasil[56] = msg.substring(508, 517);
                hasil[57] = msg.substring(517, 525);
                hasil[58] = msg.substring(525, 533);
                hasil[59] = msg.substring(533, 541);
                hasil[60] = msg.substring(541, 549);
                hasil[61] = msg.substring(549, 557);
                hasil[62] = msg.substring(557, 564);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return hasil;
    }

    public Map<String, String> getBitmap(String data) throws ISOException {
        Map<String, String> bitMap = new HashMap<String, String>();
        try {
            System.out.println("getting bitmap");
            // Create Packager based on XML that contain DE type
            

            GenericPackager packager = new GenericPackager("isoSLSascii.xml");
//            GenericPackager packager = new GenericPackager("iso87ascii.xml");
//            GenericPackager packager = new GenericPackager("iso87ascii_JPA.xml");

            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);

            isoMsg.unpack(data.getBytes());
    //        System.out.println(isoMsg.getString("39"));
            //String fieldDescription = packager.getFieldDescription(isoMsg, 1);
            // print the DE list
            //logISOMsg(isoMsg);
            // print the DE list
            //logISOMsg(isoMsg);
    //        System.out.println("======> "+isoMsg.getString(-1));
            System.out.println("======> "+isoMsg.getValue(-1));
            //isoMsg.get
            String bitmapString = isoMsg.getValue(-1).toString();
            bitmapString = bitmapString.replace("}", "");
            bitmapString = bitmapString.replace("{", "");
            String[] bitmapArray = bitmapString.split(",");

            System.out.println("=================DATA FIELD PARSING=====================");
            System.out.println(isoMsg.getString("48"));
            bitMap.put("MTI", isoMsg.getMTI());
            System.out.println("MTI " + bitMap.get("MTI"));

            for (int i = 0; i < bitmapArray.length; i++) {
                String bitId = bitmapArray[i].trim();

                bitMap.put(bitId, isoMsg.getString(bitId));
                System.out.println("BIT " + bitId + " : " + bitMap.get(bitId) + ":" + bitMap.get(bitId).length());
            }
            System.out.println("=================GETBITMAP=====================");
            return bitMap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        
//        return "";
        return bitMap;
        //return bitmapArray;
    }
}
