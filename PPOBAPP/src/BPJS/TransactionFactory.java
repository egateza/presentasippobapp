/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BPJS;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;
import java.io.IOException;
import java.util.Map;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import utility.ParseISOMessage;
import utility.Settings;

/**
 *
 * @author egateza
 */
public class TransactionFactory implements Runnable {
    private static Logger logger = Logger.getLogger(TransactionFactory.class);
    String[] arrayQueue = new String[8];
    String serializeQueue = "";
    Settings setting = new Settings();
    int inbox_id = 0;
    
    
    public TransactionFactory(String serializeQueue) {
        System.out.println(". . . Transaction Factory . . .");
        this.serializeQueue = serializeQueue;
        arrayQueue = serializeQueue.split(";");

        this.inbox_id = Integer.parseInt(arrayQueue[0]);
        setting.setSetting();

    }
    
    
    @Override
    public void run() {
        process();
    }
    
    public synchronized void process() {
        String hasil = "";
        boolean isErrorOnProcess = false;
        Bpjs bpjs = new Bpjs();
        System.out.println(". . . Process . . .");
        for (int i=0; i<arrayQueue.length; i++) {
            logger.log(Level.INFO, "array :: "+i+" :: "+arrayQueue[i]);
        }
        try {
            String RC = "";
            String detail[] = new String[9];
            //inbox message
            detail[0] = arrayQueue[1];
            //inbox sender
            detail[1] = arrayQueue[2];
            //inbox media type id
            detail[2] = arrayQueue[3];
            //inbox id
            detail[3] = String.valueOf(inbox_id);
            //inbox receiver
            detail[4] = arrayQueue[4];
            //inbox user id
            detail[5] = arrayQueue[5];
            //inbox sender type
            detail[6] = arrayQueue[6];
            //inbox id payment
            detail[7] = arrayQueue[8];
            
            String[] message = detail[0].split("\\.");
            String pelanggan = message[2];
            String idpel = pelanggan.substring(2, pelanggan.length());
            String periode = pelanggan.substring(0, 2);
            if (message[0].equals("cek")) {
                try {
                    if (Integer.parseInt(periode) < 1) {
                        
                    } else {
                        String hasilInq = bpjs.request_inquiry(idpel, periode, detail[5], String.valueOf(inbox_id));
                        logger.log(Level.INFO, "ISO INQUIRY :: "+hasilInq);
                        ParseISOMessage iSOMessage = new ParseISOMessage();
                        Map<String, String> bitMapNyo = null;
                        try {
                            bitMapNyo = iSOMessage.getBitmap(hasilInq);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                        RC = bitMapNyo.get("39");
                        BpjsProcessing post = new BpjsProcessing();
                        hasil = post.processInquiryBpjs(hasilInq);
                    }
                } catch (Exception e) {
                    isErrorOnProcess = true;
                    e.printStackTrace();
                }
            } else if (message[0].equals("inqpay") || message[0].equals("byr")){
                System.out.println("Masuk InqPay . . .");
                String hasilInq = bpjs.request_inquiry(idpel, periode, detail[5], String.valueOf(inbox_id));
                logger.log(Level.INFO, "ISO INQUIRY :: "+hasilInq);
                ParseISOMessage iSOMessage = new ParseISOMessage();
                Map<String, String> bitMapNyo = null;
                try {
                    bitMapNyo = iSOMessage.getBitmap(hasilInq);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                String isoPayment = bpjs.request_payment(bitMapNyo.get("4"), detail[7], bitMapNyo.get("48"), bitMapNyo.get("63"));
                try {
                    bitMapNyo = iSOMessage.getBitmap(isoPayment);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                RC = bitMapNyo.get("39");
                BpjsProcessing post = new BpjsProcessing();
                hasil = post.processPaymentBpjs(bitMapNyo.get("11"),isoPayment);
                System.out.println("XML :: "+hasil);
            }
            
            if (isErrorOnProcess == false) {
                insertToQueue(hasil, detail[1],
                    detail[6], String.valueOf(this.inbox_id), Integer.parseInt(detail[5]),
                    detail[4], Integer.parseInt(detail[2]), RC);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    
    private void insertToQueue(String msg, String receiver, String receiver_type, String transaction_id, int user_id, String sender, int media_type_id, String response_code) {

        String QUEUE_NAME = "outbox";
        String TASK_QUEUE_NAME = "outbox_queue";
        String EXCHANGE_NAME = "outbox_general_exchange";
        String ROUTING_KEY = "outbox.general";

        Settings setting = new Settings();
        //setting.setConnections();

        com.rabbitmq.client.Connection connection = null;
        Channel channel = null;
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(setting.getRabbit_host());
            connection = factory.newConnection();
            channel = connection.createChannel();

            String isXml = "";
            String message = receiver + "#" + msg;

            if (sender.contains("NONXML")) {
                message = receiver + "#"+msg;
                isXml = "NONXML";
            } else {
                QUEUE_NAME = "outboxxml";
                TASK_QUEUE_NAME = "outboxxml_queue";
                EXCHANGE_NAME = "outboxxml_general_exchange";
                ROUTING_KEY = "outboxxml.general";
                isXml = "XML";
            }
            message += "#" + media_type_id;
            channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
            channel.queueDeclare(TASK_QUEUE_NAME, true, false, false, null);

            if (!message.equalsIgnoreCase("0")) {

                logger.log(Level.INFO, "MASUKKAN KE DALAM QUEUE");
                channel.basicPublish(EXCHANGE_NAME, ROUTING_KEY,
                        MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());

                logger.log(Level.INFO, QUEUE_NAME + ";" + TASK_QUEUE_NAME + ";" + EXCHANGE_NAME + ";" + ROUTING_KEY + isXml);
                logger.log(Level.INFO, " [x] Sent '" + message + "'");
            }

        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, e);
            logger.log(Level.FATAL, e.getMessage());
        } finally {
            try {
                if (channel.isOpen()) {
                    channel.close();
                }
            } catch (IOException ex) {
                logger.log(Level.FATAL, ex);
            }
            try {
                if (connection.isOpen()) {
                    connection.close();
                }
            } catch (IOException ex) {
                logger.log(Level.FATAL, ex);
            }
        }
    }
    
    
    
   
}
