/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppobapp;

import BPJS.TransactionFactory;
import channel.SymphoniChannel;
import com.rabbitmq.client.*;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jpos.iso.ISOChannel;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.ISOPackager;
import org.jpos.iso.channel.NACChannel;
import org.jpos.iso.packager.GenericPackager;
import static utility.HitGateway.printWriter;
import utility.ParseISOMessage;
import utility.Settings;

/**
 *
 * @author egateza
 */
public class Main {

    /**
     * @param args the command line arguments
     */
//    public static void main(String[] args) {
//        System.out.println(String.format("%18s", "0000002426690259").replace(" ", "0"));
//        // TODO code application logic here
////        String ISO48 = "SWID003538710745406101                                JOKO NOVANDA                                  R1M000000900000000000201809                000000218632000000000000000000000000000000000000000000000000000000000000000000000000000000000";
////        System.out.println("ISI BIT48 :: "+ISO48);
////        PostpaidProcessing post = new PostpaidProcessing();
////        String[] bit48 = post.parseBit48Inquiry(ISO48, "0000");
////        System.out.println("==========================");
////        System.out.println("IDPEL :: "+bit48[1]);
////        post.insertToDb(bit48[1]);
//        try {
//            String tgl = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
//            ISOPackager packager = new GenericPackager("packager/isoSLSascii.xml");
//            ISOMsg isoMsg = new ISOMsg();
//            isoMsg.setPackager(packager);
//            
//            isoMsg.setMTI("2100");
//            isoMsg.set(2, "99508");
////            isoMsg.set(2, "00339");
//            isoMsg.set(11, String.format("%12s", "0123").replace(' ', '0'));
//            isoMsg.set(12, tgl);
//            isoMsg.set(26, "6021");
//            isoMsg.set(32, "4510017");
//            isoMsg.set(33, "4510206");
//            isoMsg.set(41, "0000000000000048");
//            
//            
//            String bit48 = "0000000"+String.format("%-18s", "0000002426690250")+ "01";
//            isoMsg.set(48, bit48);
//            String[] userData = getUserdata();
//            String bit63 = String.format("%-32s", userData[0])
//                    + String.format("%-30s", userData[1])
//                    + String.format("%-50s", userData[2])
//                    + String.format("%-18s", userData[3])
//                    + String.format("%-4s", userData[6].replace(".", ""))
//                    + String.format("%-30s", userData[5].replace(".", ""))
//                    + String.format("%-10s", userData[7].replace(".", ""))
//                    ;
//            isoMsg.set(63, bit63);
//            
//            byte[] datax = isoMsg.pack();
//            String networkRequest = new String(datax);
//            String response = printWriter(networkRequest);
////            String response = "211050300041828100020599508360000000024250000000000012320190611110728602107451001707451020800000000000000000048257JTL53L30000002426690250  01OIEJ HAIJ LIEN       (PST:  1)1101  SEMARANG                                                                                            000000240000000000002500000000242500000000000000CE431880DA7F588F                20180409080812174pids16060001                    egateza                       Kp. Kadubuluh Desa Sukasari Kecamatan Kaduhejo Pan08976588098       3601PANDEGLANG                    42252         ";
//            System.out.println("RESPONSE :: "+response);
//            ParseISOMessage iSOMessage = new ParseISOMessage();
//            Map<String, String> bitMapNyo = null;
//            try {
//                bitMapNyo = iSOMessage.getBitmap(response);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//            PostpaidProcessing post = new PostpaidProcessing();
//            String[] va = post.parseBit48Inquiry(bitMapNyo.get("48"), bitMapNyo.get("39"));
//            System.out.println("IDPEL :: "+va[1]);
//            System.out.println("XML :: "+post.processInquiryBpjs(response));
//            post.insertToDb(va[1], response, post.processInquiryBpjs(response));
//        } catch (Exception e) {
//            e.printStackTrace();
//            System.err.println(e.getMessage());
//        }
//        
//    }
    ExecutorService executor = null;
    Settings setting = new Settings();
    private static Logger logger = Logger.getLogger("Main");
    public static void main(String[] args) {

        try {
            Main main = new Main();
            main.consumer();
        } catch (Exception e) {
            e.printStackTrace();
//            logger.log(Level.FATAL, e.getMessage());
        }

    }
    
    public void consumer() {
        setting.setSetting();
        String queueCatch = "";
        Connection connection = null;
        Channel channel = null;
        try {
            
            executor = Executors.newFixedThreadPool(setting.getNTHREDS()); 

            String queueName = setting.getRabbit_queueName();
            String bindingKey = setting.getRabbit_bindingKey();

            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(setting.getRabbit_host());
            logger.log(Level.INFO, "[*] Rabbit host : "+setting.getRabbit_host());
            logger.log(Level.INFO, "[*] Rabbit queue name  : "+queueName);
            logger.log(Level.INFO, "[*] Rabbit binding key  : "+bindingKey);
            logger.log(Level.INFO, "[*] Rabbit Exchange  : "+setting.getRabbit_exchange());
            connection = factory.newConnection();
            channel = connection.createChannel();

            channel.exchangeDeclare(setting.getRabbit_exchange(), "topic", true);
            channel.queueDeclare(queueName, true, false, false, null);
            channel.queueBind(queueName, setting.getRabbit_exchange(), bindingKey);

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");
            logger.log(Level.INFO, "Waiting for messages");

            QueueingConsumer consumer = new QueueingConsumer(channel);
            channel.basicConsume(queueName, true, consumer);

            while (true) {
                try {

                    QueueingConsumer.Delivery delivery = consumer.nextDelivery();
                    String message = new String(delivery.getBody());
                    String routingKey = delivery.getEnvelope().getRoutingKey();

                    ////////////////                    
                    queueCatch = message;                    
                    Runnable worker = new TransactionFactory(message);
                    executor.execute(worker);
                    ////////////////
                    
                    logger.log(Level.INFO, " [x] Received '" + routingKey + "':'" + message + "'");
                    
                } catch (Exception e) {
                    logger.log(Level.FATAL, "FATAL MAIN :: LOOP :: CATCH :: " + queueCatch);
                    logger.log(Level.FATAL, e.getMessage());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.FATAL, "FATAL MAIN :: CATCH :: " + queueCatch);
            logger.log(Level.FATAL, e.getMessage());
        } finally {
            logger.log(Level.FATAL, "FATAL MAIN :: FINALLY :: SHUTDOWN :: " + queueCatch);
            if (connection != null) {
                try {
                    connection.close();
                } catch (Exception ignore) {
                }
            }
            executor.shutdown();
        }
    }
    
    
    
}
