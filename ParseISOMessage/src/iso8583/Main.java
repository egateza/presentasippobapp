
package iso8583;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOMsg;
import org.jpos.iso.packager.GenericPackager;

import java.io.*;
import java.net.*;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 *
 * @author ngonar
 */
public class Main {

    private final static String URL_SLS = "123.231.253.90";
    private final static Integer PORT_SERVER = 8818;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) { // Create Packager based on XML that contain DE type
        try {
           // GenericPackager packager = new GenericPackager("iso93ascii2.xml");
            //GenericPackager packager = new GenericPackager("iso87ascii.xml");
            GenericPackager packager = new GenericPackager("isoSLSascii.xml");
                        
            //Bitmap : {2, 3, 7, 11, 12, 13, 15, 18, 32, 41, 42, 48, 49}
            // Create ISO Message
            ISOMsg isoMsg = new ISOMsg();
            isoMsg.setPackager(packager);
//            isoMsg.setMTI("2200");
//            isoMsg.set(2, "074002");
//            isoMsg.set(3, "380000");
//            isoMsg.set(7, "0930162847");
//            isoMsg.set(11, "172869");
//            isoMsg.set(12, "162847");
//            isoMsg.set(13, "0930");
//            isoMsg.set(15, "0930");
//            isoMsg.set(18, "6012");
//            isoMsg.set(32, "037");
//            isoMsg.set(41, "DEVELPLG");
//            isoMsg.set(42, "200900100800000");
//            isoMsg.set(48, "60144465");
//            isoMsg.set(49, "360");
            
            
            isoMsg.setMTI("2110");
            isoMsg.set(2,"99502");            
            isoMsg.set(11,"443523");            
            isoMsg.set(26,"6012");
            isoMsg.set(39,"0068");
            
            
            
            
           /* isoMsg.set(33, "4510103");
            isoMsg.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
            isoMsg.set(33, "4510103");
            isoMsg.set(40, "001");
            isoMsg.set(41, "PLG0000000000001");
            isoMsg.set(48, "0000000");*/
                      

            /////inquiry
            /*isoMsg.setMTI("2100");
            isoMsg.set(2,"99502");
            isoMsg.set(11,"000000143388");
            isoMsg.set(12,"20110201065634");
            isoMsg.set(26,"6012");
            isoMsg.set(32,"0000000");
            isoMsg.set(33,"0090101");
            isoMsg.set(41,"00JTM1504ZMSDY06");
            isoMsg.set(48,"0000000011121503780000000000000");*/

            /*isoMsg.setMTI("2200");
            isoMsg.set(2,"99502");
            isoMsg.set(4,"3600000000020000");
            isoMsg.set(11,"000000143388");
            isoMsg.set(12,"20110201065644");
            isoMsg.set(26,"6012");
            isoMsg.set(32,"0000000");
            isoMsg.set(33,"0090101");
            isoMsg.set(41,"00JTM1504ZMSDY06");
            isoMsg.set(48,"00000000111200037851121053440001A95B976BE284AA487E35B2D1F858A4C0A371166665794561687913149204940H.RAHMAD                 R100000000900200000016000");
            isoMsg.set(62,"515112112300648");*/

            // print the DE list
            //logISOMsg(isoMsg);

            // Get and print the output result
            byte[] datax = isoMsg.pack();
            System.out.println("RESULT : " + new String(datax));

            /////////////////////////////////////////////////

           // System.out.println(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));

            // membuat channel
//            ASCIIChannel channel = new ASCIIChannel(URL_SLS, PORT_SERVER, packager);
//
//            ISOMUX isoMux = new ISOMUX(channel) {
//                @Override
//                protected String getKey(ISOMsg m) throws ISOException {
//                    return super.getKey(m);
//                }
//            };
//
//            new Thread(isoMux).start();
//
//            // bikin network request
//            ISOMsg networkReq = new ISOMsg();
//
//            /*networkReq.setMTI("1800");
//            networkReq.set(3, "123456");
//            networkReq.set(7, new SimpleDateFormat("yyyyMMdd").format(new Date()));
//            networkReq.set(11, "000001");
//            networkReq.set(12, new SimpleDateFormat("HHmmss").format(new Date()));
//            networkReq.set(13, new SimpleDateFormat("MMdd").format(new Date()));
//            networkReq.set(48, "Tutorial ISO 8583 Dengan Java");
//            networkReq.set(70, "001");*/
//
//           /* networkReq.setMTI("2800");
//            networkReq.set(12, new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
//            networkReq.set(33, "4410101");
//            networkReq.set(40, "001");
//            networkReq.set(41, "SLS0000000000001");
//            networkReq.set(48, "ST145S3");*/
//
//            networkReq.setPackager(packager);
//            byte[] datax = isoMsg.pack();
//            System.out.println("REQ : " + new String(datax));
//
//            ISORequest req = new ISORequest(isoMsg);
//            isoMux.queue(req);
//
//            ISOMsg reply = req.getResponse(5*100);
//            if (reply != null) {
//                System.out.println("Req ["+new String(networkReq.pack()) + "]");
//                System.out.println("Res ["+new String(reply.pack()) + "]");
//            }
//
//            isoMux.terminate();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private static void logISOMsg(ISOMsg msg) {
            System.out.println("----ISO MESSAGE-----");
            try {
                    System.out.println("  MTI : " + msg.getMTI());
                    for (int i=1;i<=msg.getMaxField();i++) {
                            if (msg.hasField(i)) {
                                    System.out.println("    Field-"+i+" : "+msg.getString(i));
                            }
                    }
            } catch (ISOException e) {
                    e.printStackTrace();
            } finally {
                    System.out.println("--------------------");
            }

    }

}
