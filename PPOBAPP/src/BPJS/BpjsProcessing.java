/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BPJS;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import model.Inboxes;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import utility.ParseISOMessage;

/**
 *
 * @author egateza
 */
public class BpjsProcessing {
    private static Logger logger = Logger.getLogger(BpjsProcessing.class);
    public static String[] parseBit48Inquiry(String msg, String rc) {
        String[] hasil = new String[65];

        int[] seq = {7, 18, 2, 30, 6, 100, 12, 12, 12, 12};

        String[] title = {
            "switcher id",
            "No VA",
            "jumlah bulan",
            "nama",
            "kode cabang",
            "nama cabang",
            "biaya premu",
            "biaya admin",
            "Rp. Total",
            "sisa sebelumnya",};
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return hasil;
    }
    
    public String[] parseBit63Payment(String msg, String rc) {

        String[] hasil = new String[65];

//        int[] seq = {32, 30, 50, 18, 4};
        int[] seq = {32, 30, 50, 18, 4, 30, 10};

        String[] title = {
            "kode_loket",
            "nama_loket",
            "alamat_loket",
            "phone_loket",
            "kode_kab_kota",
            "kab_kota",
            "kode_pos"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }
    
    public String[] parseBit48Payment(String msg, String rc) {

        String[] hasil = new String[65];

        int[] seq = {7, 18, 2, 30, 6, 100, 12, 12, 12, 12, 32, 14};

        String[] title = {
            "switcher id",
            "No VA",
            "jumlah bulan",
            "nama",
            "kode cabang",
            "nama cabang",
            "biaya premu",
            "biaya admin",
            "Rp. Total",
            "sisa",
            "JPA Reff",
            "Tgl Lunas"
        };
        try {
            int n = 0;
            int f = 0;
            int l = 0;
            for (int i = 0; i < seq.length; i++) {
                hasil[i] = "";
                if (rc.equalsIgnoreCase("0000")) {
                    if (seq[i] == 1) {
                        hasil[i] = String.valueOf(msg.charAt(l));
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                        l += seq[i];
                    } else if (seq[i] > 1) {
                        f = n;
                        l = n + seq[i];
                        hasil[i] = msg.substring(f, l);
//                        logger.log(Level.INFO, String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                    }
                }
                System.out.println(String.format("%-50s", title[i]) + " : " + String.format("%-30s", hasil[i]) + " ==> " + hasil[i].length());
                n = l;
            }

        } catch (Exception e) {
            logger.log(Level.FATAL, e.getMessage());
            e.printStackTrace();
        }

        return hasil;
    }
    
    public void insertToDb(String idpel, String Isomsg, String xml) {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("PPOBAPPPU", System.getProperties());
        EntityManager em = factory.createEntityManager();
        try {
            Inboxes inbox = new Inboxes();
            em.getTransaction().begin();

            inbox.setIdpel(idpel);
            inbox.setIso(Isomsg);
            inbox.setXml(xml);
            em.persist(inbox);
            em.getTransaction().commit();
            em.close();
            factory.close();
        } catch (Exception e) {
            System.err.println("gagal menyimpan");
            e.printStackTrace();
            System.err.println(e.getMessage());
            System.err.println(e);
        } finally {
            System.err.println("finally");

            try {
                em.close();
            } catch (Exception e) {
                System.err.println("EntityManager already closed");
            }
            try {
                factory.close();
            } catch (Exception e) {
                System.err.println("EntityManagerFactory already closed");
            }
        }
        
    }
    
    
    public String processInquiryBpjs(String hasil) {
        //String[] result = post.parseInquiryMsgResponse(hasil, false);
//        GenericResponseCode genericResponseCode = new GenericResponseCode();
        ParseISOMessage iSOMessage = new ParseISOMessage();
        Map<String, String> bitMapNyo = null;
        try {
            bitMapNyo = iSOMessage.getBitmap(hasil);
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.log(Level.FATAL, ex.getMessage());
//            java.util.logging.Logger.getLogger(PDAM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        String[] datas = new String[16];
        Map<String, String> tagMap = new HashMap<String, String>();
//        tagMap.put("MTI", "trx_type");
        tagMap.put("2", "pan");
//            tagMap.put("3", "processing_code");
        tagMap.put("4", "amount");
//            tagMap.put("7", "transmission_date_time");
        tagMap.put("11", "stan");
        tagMap.put("12", "date_time");
//            tagMap.put("13", "local_trx_date");
//            tagMap.put("15", "settlement_date");
        tagMap.put("26", "merchant_type");
        tagMap.put("32", "bank_code");
        tagMap.put("33", "cid");
//            tagMap.put("37", "retrieval_ref_no");
        tagMap.put("39", "rc");
        tagMap.put("41", "terminal_id");
//        tagMap.put("42", "acceptor_identification_code");
        tagMap.put("48", "private_data_48");

//        String[] activeBit = {"MTI", "2", "4", "11", "12", "26", "32", "33", "39", "41", "48"};
        String[] activeBit = {"4", "11", "12", "26", "32", "39", "41", "48"};

        String xml = "<?xml version='1.0' encoding='UTF-8'?><response>";
        xml += "<trx_type>INQUIRY</trx_type>";
        xml += "<product_type>BPJS-KESEHATAN</product_type>";
        xml += "<product_code>BPJS-KESEHATAN</product_code>";

//        String[] result = pdam.parseInquiryMsgResponse(bitMapNyo, false);
        if (bitMapNyo.get("39").equalsIgnoreCase("0000")) {

            for (int i = 0; i < activeBit.length; i++) {
//                String activeBit1 = activeBit[i];
                if (activeBit[i].equalsIgnoreCase("4")) {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]).substring(4) + "</" + tagMap.get(activeBit[i]) + ">";
                } else {
                    xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                }
            }   
            String[] bit48 = parseBit48Inquiry(bitMapNyo.get("48"), bitMapNyo.get("39"));
            String[] titleMain = {
                "switcher_id",
                "no_va",
                "jumlah_bulan",
                "nama",
                "kode_cabang",
                "nama_cabang",
                "biaya_premi",
                "biaya_admin",
                "amount",
                "sisa"};

            int mainData = 7;
            int detailData = 4;
            int indexData = 0;
//            for (int i = 0; i < mainData; i++) {
            for (int i = 0; i < titleMain.length; i++) {
                indexData = i;
                if (!titleMain[i].equalsIgnoreCase("amount")
                        && !titleMain[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                        ) {
                    xml += "<" + titleMain[i] + ">" + bit48[i] + "</" + titleMain[i] + ">";
                }
            }
            xml += "<cmd></cmd>";
            System.out.println("xml response : ");
            
//            logger.log(Level.INFO, xml);
            System.out.println(xml);
        } else {
            System.out.println("++++++++++++++kena dua++++++++++++++++++++");
//            xml = "<?xml version='1.0' encoding='UTF-8'?>"
            //String[] bit48 = pdam.parseBit48Inquiry(bitMapNyo.get("48"), bitMapNyo.get("39"));
            xml += "<stan>" + bitMapNyo.get("11") + "</stan>";
            xml += "<rc>" + bitMapNyo.get("39") + "</rc>";
            xml += "<code>" + bitMapNyo.get("39") + "</code>";
//            xml += "<desc>" + genericResponseCode.getResponseDesctiprionBpjs(bitMapNyo.get("39")) + "</desc>";
            xml += "<desc></desc>";
//                String idpel = bit48[0];
        }

        xml += "</response>";

//                System.out.println("++++++++++++++kena tiga++++++++++++++++++++");
//        xml.replace("norek_val", bitMapNyo.get("48").substring(0,8));
        String nn = bitMapNyo.get("48").substring(0, 7);
        xml = xml.replace("norek_val", nn);
        return xml;
    }
    
    public String processPaymentBpjs(String inbox_id, String hasil) {
        String hasilnyo = "";
        ParseISOMessage iSOMessage = new ParseISOMessage();
        Map<String, String> bitMapNyo = null;
        try {
            bitMapNyo = iSOMessage.getBitmap(hasil);
        } catch (Exception ex) {
            logger.log(Level.FATAL, ex.getMessage());
//            java.util.logging.Logger.getLogger(PDAM.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        String[] datas = new String[16];
        Map<String, String> tagMap = new HashMap<String, String>();
        tagMap.put("MTI", "mti");
        tagMap.put("2", "pan");
//            tagMap.put("3", "processing_code");
        tagMap.put("4", "amount");
//            tagMap.put("7", "transmission_date_time");
        tagMap.put("11", "stan");
        tagMap.put("12", "date_time");
//            tagMap.put("13", "local_trx_date");
//            tagMap.put("15", "settlement_date");
        tagMap.put("26", "merchant_type");
        tagMap.put("32", "bank_code");
        tagMap.put("33", "cid");
//            tagMap.put("37", "retrieval_ref_no");
        tagMap.put("39", "rc");
        tagMap.put("41", "terminal_id");
//        tagMap.put("42", "acceptor_identification_code");
        tagMap.put("48", "private_data_48");
        tagMap.put("62", "info_text");
//        tagMap.put("63", "info_loket");

//        String[] activeBit = {"MTI", "2", "4", "11", "12", "26", "32", "33", "39", "41", "48"};
        String[] activeBit = {"4", "11", "12", "26", "32", "39", "41", "62", "48"};

        String xml = "<?xml version='1.0' encoding='UTF-8'?><response>";
        xml += "<trx_type>PAYMENT</trx_type>";
        xml += "<product_type>BPJS-KESEHATAN</product_type>";
        xml += "<product_code>BPJS-KESEHATAN</product_code>";
        xml += "<stan>" + inbox_id + "</stan>";
//        xml += "<kode_loket>" + userData[0] + "</kode_loket>";

//        String[] result = pdam.parseInquiryMsgResponse(bitMapNyo, false);
        if (bitMapNyo.get("39").equalsIgnoreCase("0000")) {
            try {
                for (int i = 0; i < activeBit.length; i++) {
                    String activeBit1 = activeBit[i];
    //                xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                    if (activeBit[i].equalsIgnoreCase("4")) {
                        System.out.println("AKTIF BIT :: "+activeBit[i]);
                        xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]).substring(4) + "</" + tagMap.get(activeBit[i]) + ">";
                    } else {
                        xml += "<" + tagMap.get(activeBit[i]) + ">" + bitMapNyo.get(activeBit[i]) + "</" + tagMap.get(activeBit[i]) + ">";
                    }

    //                if (activeBit[i].equalsIgnoreCase("11")) {
    //                    xml += "<" + tagMap.get(activeBit[i]) + ">" + inboxIdPelangi + "</" + tagMap.get(activeBit[i]) + ">";
    //                }
                }

                String[] bit48 = parseBit48Payment(bitMapNyo.get("48"), bitMapNyo.get("39"));
                String[] titleMain = {
                    //                "idpel", "blth", "name", "bill_count", "bill_repeat_count", "rp_tag", "biaya_admin"
                    "switcher_id",
                    "no_va",
                    "jumlah_bulan",
                    "nama",
                    "kode_cabang",
                    "nama_cabang",
                    "biaya_premi",
                    "biaya_admin",
                    "amount",
                    "sisa",
                    "reffno",
                    "tgl_lunas"
                };
    //            String[] titleDetail = {
    //                "bill_date", "bill_amount", "penalty", "kubikasi"
    //            };

                int mainData = 7;
                int detailData = 4;
                int indexData = 0;
                long biayaAdmin = 0;
                long biayaPremi = 0;
                long amount = 0;
    //            for (int i = 0; i < mainData; i++) {
                for (int i = 0; i < titleMain.length; i++) {
                    indexData = i;
                    if (!titleMain[i].equalsIgnoreCase("amount")
                            && !titleMain[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                            ) {
                        if (!titleMain[i].equalsIgnoreCase("amount")) {
                            xml += "<" + titleMain[i] + ">" + bit48[i] + "</" + titleMain[i] + ">";

                            if (titleMain[i].equalsIgnoreCase("biaya_admin")) {
                                biayaAdmin = Long.parseLong(bit48[i]);
                            }
                            if (titleMain[i].equalsIgnoreCase("biaya_premi")) {
                                biayaPremi = Long.parseLong(bit48[i]);
                            }

                        }
                    }
                    if (titleMain[i].equalsIgnoreCase("amount")) {
                        amount = Long.parseLong(bit48[i]);
                    }

                }

                //
                String[] bit63 = parseBit63Payment(bitMapNyo.get("63"), bitMapNyo.get("39"));
                String[] titleMain63 = {
                    //                "idpel", "blth", "name", "bill_count", "bill_repeat_count", "rp_tag", "biaya_admin"
                    "kode_loket",
                    "nama_loket",
                    "alamat_loket",
                    "phone_loket",
                    "kode_kab_kota",
                    "kab_kota",
                    "kode_pos"
                };

                mainData = 7;
                detailData = 4;
                indexData = 0;
    //            for (int i = 0; i < mainData; i++) {
                for (int i = 0; i < titleMain63.length; i++) {
                    indexData = i;
                    if (!titleMain63[i].equalsIgnoreCase("amount")
                            && !titleMain63[i].equalsIgnoreCase("switcher_id") //                        && !titleMain[i].equalsIgnoreCase("private_data_48")
                            ) {
                        if (!titleMain63[i].equalsIgnoreCase("amount")) {
                            xml += "<" + titleMain63[i] + ">" + bit63[i] + "</" + titleMain63[i] + ">";
                        }
                    }
                }

                try {
                    xml += "<cust_msisdn>000</cust_msisdn>";
                } catch (Exception e) {
                    xml += "<cust_msisdn></cust_msisdn>";
                }
                xml += "<saldo>0</saldo>";
                xml += "<harga>" + bitMapNyo.get("4").substring(4) + "</harga>";
                xml += "</response>";
                hasilnyo = xml;
            }catch (Exception e) {
                System.out.println("Masuk catch"+e );
                e.printStackTrace();
            }
            
        }
        return hasilnyo;
    }
}
